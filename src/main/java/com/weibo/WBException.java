package com.weibo;

/**
 * 微博异常，对于部分error直接抛出异常
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2014-4-17 上午11:12:57
 */
public class WBException extends Exception {

	private static final long serialVersionUID = -9173555997504771331L;

	public WBException() {}

	public WBException(String message, Throwable cause) {
		super(message, cause);
	}

	public WBException(String message) {
		super(message);
	}

	public WBException(Throwable cause) {
		super(cause);
	}
}
