package com.weibo;

/**
 * 回复信息的类型
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2014-4-22 下午4:07:59
 */
public enum WBReMsgTypes {

	TEXT("text"), 			// 1、纯文本类型私信消息：text
	ARTICLES("articles"), 	// 2、图文类型私信消息：articles
	POSITION("position"); 	// 3、位置类型私信消息：position

	private String type;
	
	WBReMsgTypes(String type) {
		this.type = type;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
