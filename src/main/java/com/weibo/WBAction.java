package com.weibo;

import com.weibo.bean.WBMsg;
import com.weibo.bean.WBMsgData;
import com.weibo.bean.WBReMsg;

/**
 * 处理微博的消息
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2014-4-22 上午11:02:38
 */
public interface WBAction {
	WBReMsg _do(WBMsg<WBMsgData> msg);
}