package com.weibo.api;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * 微博支付api
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2014-4-15 上午10:36:28
 */
public class WBPayment {

	// 订单信息地址
	private static final String _URL_PAYMENT = "http://weibo.cn/weibobrowser/payment/order";
	
	public WBPayment() {}

	public static WBPayment me() {
		return new WBPayment();
	}
	
	/*
	source			必须	string		在微博开放平台申请应用时分配的AppKey，调用接口时候代表应用的唯一身份
	seller_id		必须	string		卖家微博uid
	sign_type		必须	string		签名方式，默认对称md5
	sign			必须	string		签名，根据url参数以及密钥生成, 详见“六. 签名机制”
	notify_url		可选	string		支付结果异步通知路径（服务器端通知）
	show_url		可选	string		支付完成跳转页面（完成支付回调页面）
	out_trade_no	必须	string		商户业务单据编号，或微博订单号
	subject			必须	string		交易内容：标题
	body			可选	string		交易内容：描述
	pay_channel		必选	int(4)		支付渠道，必须填写，扫码支付为101，其他支付渠道为100，
	pay_type		可选	int(4)		支付业务类型，根据业务规划给出参数列表，如1=实物商品售卖，2=虚拟商品售卖，3=捐助，4=权益等
//	price			可选	float(10,2)	单价，与购买数量是成对出现的参数，与交易金额不能同时存在
//	quantity		可选	int(8)		购买数量，与单价是成对出现的参数，与交易金额不能同时存在
	total_fee		可选	float(10,2)	交易金额，支付请求的金额，与单价、数量不能同时存在
	it_b_pay		可选	string		超时时间，交易请求自创建时间起，自动关闭的时间，取值范围：1m(分钟)、1h（小时）、1c(当天0点)、15d(天)，不接受小数点，默认3d
	extra			可选	string		额外参数，发起支付时透传的参数，可为不包含"="、"&"等特殊字符的字符串
	royalty_type	（待开发）		可选	string	默认为10，royalty_parameters有值时不能为空
	royalty_parameters 	（待开发）	可选	string	分润账号集，详见“七. 分润机制”
	*/
	/**
	 * 构造支付链接
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public String initPayUrl(String out_trade_no, String total_fee, String subject, String body) throws UnsupportedEncodingException {
		Map<String, String> params = new HashMap<String, String>();
		// 固定配置
		params.put("source", WeiBo.props.get("app_id"));
		params.put("seller_id", WeiBo.props.get("seller_id"));
		params.put("notify_url", WeiBo.props.get("notify_url"));
		params.put("show_url", WeiBo.props.get("show_url"));
		// 其他参数
		params.put("out_trade_no", out_trade_no);
		params.put("subject", subject);
		params.put("body", body);
		params.put("total_fee", total_fee);
		params.put("pay_channel", "100");
		params.put("pay_type", "1");
		params.put("it_b_pay", "1d");
		params.put("extra", "yl");
		// royalty_type，royalty_parameters待开发，注释掉
//		params.put("royalty_type", "10");
//		params.put("royalty_parameters", "");
		String sign = WeiBo.initUrl(params, true) + WeiBo.props.get("secret");
		params.put("sign_type", "md5");
		params.put("sign", DigestUtils.md5Hex(sign.getBytes(WeiBo.DEFAULT_CHARSET)));
		return _URL_PAYMENT.concat("?") + WeiBo.initUrl(params, false);
	}
	
	public static void main(String[] args) throws UnsupportedEncodingException {
		System.out.println(WBPayment.me().initPayUrl("111111", "100", "永乐票务", "永乐票务"));
	}
}