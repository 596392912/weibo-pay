package com.weibo.api;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;

import com.alibaba.fastjson.JSONObject;
import com.weibo.util.HttpKit;

/**
 * 对账查询接口
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2014-4-15 下午1:38:02
 */
public class WBPay {

	// 查询接口
	private static final String _URL_QUERY  = "http://api.sc.weibo.com/v2/pay/accountquery";
	// 退款接口
	private static final String _URL_REFUND = "http://api.sc.weibo.com/v2/pay/refund";
	
	public WBPay() {}

	public static WBPay me() {
		return new WBPay();
	}

	// 请求参数
	/*
	source			必须	string	申请应用时分配的AppKey，调用接口时候代表应用的唯一身份
	uid				必须	string	商户微博uid
	sign_type		必须	string	签名方式，默认对称md5
	sign			必须	string	签名，根据url参数以及密钥生成
	out_trade_no	可选	string	商户业务单据编号，与out_trade_no、gmt_start_time、gmt_end_time不能同时存在
	gmt_start_time	可选	Date	查询需要对账开始时间，格式为“yyyy-MM-dd HH:mm:ss”
	gmt_end_time	可选	Date	查询需要对账结束时间，格式为“yyyy-MM-dd HH:mm:ss”
	trade_status	可选	string	支付状态，支付创建-WAIT_BUYER_PAY/等待支付-TRADE_PENDING/已支付-TRADE_SUCCESS/超时关闭-TRADE_CLOSED/支付结束-TRADE_FINISHED，其它TBD.为空则返回全部，否则按照支付单状态筛选.
	pageno			可选	int(4)	查询页号
	page_size		可选	int(4)	分页大小，默认20，最大100，且pageno*page_size最多5000
	*/
	public JSONObject query(String out_trade_no) throws UnsupportedEncodingException {
		Map<String, String> params = new HashMap<String, String>();
		// 固定配置
		params.put("source", WeiBo.props.get("app_id"));
		params.put("uid", WeiBo.props.get("seller_id"));
		
		params.put("out_trade_no", out_trade_no);
		String sign = WeiBo.initUrl(params, true) + WeiBo.props.get("secret");
		params.put("sign_type", "md5");
		params.put("sign", DigestUtils.md5Hex(sign.getBytes(WeiBo.DEFAULT_CHARSET)));
		String json = HttpKit.get(_URL_QUERY, params);
		return JSONObject.parseObject(json);
	}
	
	public static void main(String[] args) throws UnsupportedEncodingException {
		System.out.println(WBPay.me().query("11111"));
	}
	
	// 返回数据说明
	/*
	trade_id		支付单据编号	8xxxxxxxxxxx
	out_trade_no	商户业务单据编号	"xxxxxxxxxxxx"
	buyer_id		买家微博uid	"2950843533"
	return_url		支付完成回调页面路径	"http://xxx.xxx.com/xxx"
	notify_url		支付结果异步通知路径	"http://xxx.xxx.com/xxx"
	subject			交易内容：标题	"xxxxx"
	body			交易内容：描述	"xxxxx"
	show_url		交易内容：收银台页面上商品展示的链接	"http://xxx.xxx.com/xxx"
	pay_type		支付业务类型，根据业务规划给出参数列表，如1=实物商品售卖，2=虚拟商品售卖，3=捐助，4=权益等	1
	price			单价，与购买数量是成对出现的参数，与交易金额不能同时存在	10.00
	quantity		购买数量，与单价是成对出现的参数，与交易金额不能同时存在	5
	total_fee		交易金额，支付请求的金额，与单价、数量不能同时存在	50.00
	it_b_pay		超时时间，交易请求自创建时间起，自动关闭的时间，取值范围：1m(分钟)-1h（小时）-1c(当天0点)-15d(天)，不接受小数点，默认3d	3d
	extra			额外参数，发起支付时透传的参数，可为不包含"="、"&"等特殊字符的字符串	"test"
	status			支付状态，支付创建-WAIT_BUYER_PAY/等待支付-TRADE_PENDING/已支付-TRADE_SUCCESS/超时关闭-TRADE_CLOSED/支付结束-TRADE_FINISHED/支付失败-TRADE_FAILED，其它TBD.	TRADE_SUCCESS
	gmt_create		前提支付状态，支付请求创建的时间	"yyyy-MM-dd HH:mm:ss"
	create_time		前提支付状态，	支付时间	"yyyy-MM-dd HH:mm:ss"
	gmt_close		前提支付状态，支付请求关闭的时间	"yyyy-MM-dd HH:mm:ss"
	refund_status	退款状态，和trade_status互斥，退款中-REFUND_PENDING/退款失败-REFUND_FAILED/已退款-REFUND_SUCCESS/退款关闭-REFUND_CLOSED，其它TBD.	REFUND_SUCCESS
	gmt_refund		前提退款状态，该笔支付的退款时间	"yyyy-MM-dd HH:mm:ss"
	*/
	
	// 退款接口
	// 参数
	/*
	source		必须	string	申请应用时分配的AppKey，调用接口时候代表应用的唯一身份（目前不是必须验证的）
	uid			必须	string	商户微博uid
	sign_type	必须	string	签名方式，默认对称md5
	sign		必须	string	签名，根据url参数以及密钥生成
	refund_url	必须	string	退款结果异步通知路径
	batch_no	必须	string	退款批次号，即支付单号
	detail_data	必须	string	退款详情，第一笔交易退款数据#第二笔交易退款数据集#第三笔交易退款数据集…#第N笔交易退款数据集：交易号^退款总金额^退款理由
	batch_num	必须	int(4)	需要与退款详情中的笔数一致
	 */
	public JSONObject refund(String batch_no, String total_fee, String reason) {
		Map<String, String> params = new HashMap<String, String>();
		// 固定配置
		params.put("source", WeiBo.props.get("app_id"));
		params.put("uid", WeiBo.props.get("seller_id"));
		params.put("refund_url", WeiBo.props.get("refund_url"));
		params.put("batch_no", WeiBo.props.get("batch_no"));
		params.put("detail_data", batch_no + "^" + total_fee + "^" + reason);
		params.put("batch_num", "1");
		String json = HttpKit.get(_URL_REFUND, params);
		return JSONObject.parseObject(json);
	}
	
	// 返回数据说明
	/*
	notify_time			date	通知发送的时间。格式为：yyyy-MM-dd HH:mm:ss
	notify_type			string	通知的类型
	notify_id			string	APP key即传入的source参数
	sign				string	签名，根据url参数以及密钥生成
	sign_type			string	签名方式，默认对称md5
	batch_no			string	原请求退款批次号，退款日期+流水号（3-24位，不能为0）
	success_num			int(4)	退交易成功的笔数
	result_details		string	处理结果详情，微博交易号^退款总金额^处理结果^退款用户微博UID$微博交易号^退款总金额^处理结果^退款用户微博UID$
	*/
}
