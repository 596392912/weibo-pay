package com.weibo.util;

import java.util.Arrays;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

/**
 * 工具类
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2014-4-15 上午11:45:55
 */
public final class Tools {

	/**
	 * 校验URL有效性，类似微信，appsecret同微信token
	 * @param appsecret ：appsecret私钥
	 * @param signature ：微博加密签名，signature结合了开发者的appsecret、和请求中的timestamp参数，nonce参数
	 * @param timestamp ：时间戳
	 * @param nonce     ：随机数
	 * @param echostr   ：随机字符串
	 * @return
	 */
	public static final boolean checkSignature(String signature, String... string) {
		Arrays.sort(string);
		return DigestUtils.sha1Hex(StringUtils.join(string, "")).equals(signature);
	}
}
