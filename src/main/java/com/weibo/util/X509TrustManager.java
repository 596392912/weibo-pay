package com.weibo.util;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 *  证书管理
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2014-4-30 下午1:20:15
 */
public class X509TrustManager implements javax.net.ssl.X509TrustManager {

	@Override
	public void checkClientTrusted(X509Certificate[] chain, String authType)
			throws CertificateException {
	}

	@Override
	public void checkServerTrusted(X509Certificate[] chain, String authType)
			throws CertificateException {
	}

	@Override
	public X509Certificate[] getAcceptedIssuers() {
		return null;
	}

}
