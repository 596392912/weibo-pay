package com.weibo.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

/**
 * 配置文件
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2014-4-22 上午9:25:21
 */
public class ConfKit {

	/**
	 * 读取配置文件
	 * @param propsName
	 * @return
	 */
	public static Map<String, String> get(String propsName) {
		Map<String, String> map = new HashMap<String, String>();
		Properties properties = new Properties();
		String basePath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
		InputStreamReader inputStream = null;
		try {
			inputStream = new InputStreamReader(new FileInputStream(new File(basePath + propsName)), "UTF-8");
			properties.load(inputStream);
			for(Object key : properties.keySet()){
				String property = (String)key;
				String proValue = properties.getProperty(property);
				map.put(property, proValue);
			}
			properties = null;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(inputStream);
			inputStream = null;
		}
		return map;
	}
}
