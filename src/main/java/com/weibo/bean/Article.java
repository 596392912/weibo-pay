package com.weibo.bean;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 微博图文实体
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2014-4-22 下午1:18:20
 */
public class Article {

	@JSONField(name = "display_name")
	private String displayName;
	private String summary;
	private String image;
	private String url;

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "Article [displayName=" + displayName + ", summary=" + summary
				+ ", image=" + image + ", url=" + url + "]";
	}
}