package com.weibo.bean;

/**
 * 接收到的消息内容封装
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2014-4-16 下午5:14:56
 */
public class WBMsgData {

	private static final String _FOLLOW			= "follow";
	private static final String _UNFOLLOW		= "unfollow";
	private static final String _SUBSCRIBE		= "subscribe";
	private static final String _UNSUBSCRIBE	= "unsubscribe";
	private static final String _SCAN			= "scan";
	private static final String _SCAN_FOLLOW	= "scan_follow";
	private static final String _PAY			= "pay";
	private static final String _DRAWBACK		= "drawback";

	// follow：关注事件，unfollow：取消关注事件，subscribe：订阅事件，unsubscribe：取消订阅事件。
	// scan和scan_follow为二维码扫描事件。pay：支付, drawback：退款
	private String subtype;
	// subtype为follow、unfollow、subscribe或unsubscribe时不返回
	private String key;
	// subtype为scan和scan_follow时才返回
	private String ticket;
	// 经度
	private String longitude;
	// 纬度
	private String latitude;
	// 发送者用此ID查看
	private String vfid;
	// 接收者用此ID查看
	private String tovfid;

	public String getSubtype() {
		return subtype;
	}

	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getVfid() {
		return vfid;
	}

	public void setVfid(String vfid) {
		this.vfid = vfid;
	}

	public String getTovfid() {
		return tovfid;
	}

	public void setTovfid(String tovfid) {
		this.tovfid = tovfid;
	}
	
	public boolean ISFollow (){
		return subtype.equalsIgnoreCase(_FOLLOW);
	}
	
	public boolean ISUnfollow (){
		return subtype.equalsIgnoreCase(_UNFOLLOW);
	}
	
	public boolean ISSubscribe (){
		return subtype.equalsIgnoreCase(_SUBSCRIBE);
	}
	
	public boolean ISUnsubscribe (){
		return subtype.equalsIgnoreCase(_UNSUBSCRIBE);
	}
	
	public boolean ISScan (){
		return subtype.equalsIgnoreCase(_SCAN);
	}
	
	public boolean ISScan_follow (){
		return subtype.equalsIgnoreCase(_SCAN_FOLLOW);
	}
	
	public boolean ISPay (){
		return subtype.equalsIgnoreCase(_PAY);
	}
	
	public boolean ISDrawback (){
		return subtype.equalsIgnoreCase(_DRAWBACK);
	}
	
	@Override
	public String toString() {
		return "WBMsgData [subtype=" + subtype + ", key=" + key + ", ticket="
				+ ticket + ", longitude=" + longitude + ", latitude="
				+ latitude + ", vfid=" + vfid + ", tovfid=" + tovfid + "]";
	}
}
