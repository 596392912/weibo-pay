package com.weibo.bean;

import java.beans.Transient;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import org.apache.commons.io.FileUtils;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.annotation.JSONField;
import com.weibo.WBMsgTypes;
import com.weibo.api.WBReply;

/**
 * 消息推送封装
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2014-4-17 上午10:11:21
 */
public class WBMsg<T> {

	private String type;
	@JSONField(name = "receiver_id")
	private String receiverId;
	@JSONField(name = "sender_id")
	private String senderId;
	@JSONField(name = "created_at") // E MMM d HH:mm:ss Z yyyy
	private Date createdAt;
	private String text;
	private T data;

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getReceiverId() {
		return receiverId;
	}
	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}
	public String getSenderId() {
		return senderId;
	}
	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	@SuppressWarnings("deprecation")
	public void setCreatedAt(String createdAt) {
		this.createdAt = new Date(createdAt);
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "WBEvent [type=" + type + ", receiverId=" + receiverId
				+ ", senderId=" + senderId + ", createdAt=" + createdAt
				+ ", text=" + text + ", data=" + data + "]";
	}
	
	@Transient
	public Boolean ISText(){
		return this.type.equals(WBMsgTypes.TEXT.getType());
	}
	
	@Transient
	public Boolean ISPosition(){
		return this.type.equals(WBMsgTypes.POSITION.getType());
	}
	
	@Transient
	public Boolean ISVoice(){
		return this.type.equals(WBMsgTypes.VOICE.getType());
	}
	
	@Transient
	public Boolean ISImage(){
		return this.type.equals(WBMsgTypes.IMAGE.getType());
	}
	
	@Transient
	public Boolean ISEvent(){
		return this.type.equals(WBMsgTypes.EVENT.getType());
	}
	
	@Transient
	public Boolean ISMention(){
		return this.type.equals(WBMsgTypes.MENTION.getType());
	}

	public static void main(String[] args) throws IOException, ParseException {
		String file = "E:\\eclipse_workspace\\weibo_pay\\resources\\msg.json";
		
		String json = FileUtils.readFileToString(new File(file), "utf-8");
		
		WBMsg<WBMsgData> wb = JSONObject.parseObject(json, new TypeReference<WBMsg<WBMsgData>>(){});
		
		// 测试消息解析返回
		System.out.println(WBReply.parseMsg(wb));
	}
}
