package com.weibo.bean;

import java.beans.Transient;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;

import org.apache.commons.io.FileUtils;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.weibo.WBReMsgTypes;

/**
 * 消息推送服务-消息回复
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2014-4-17 上午10:44:10
 */
public class WBReMsg {

	private Boolean result;
	@JSONField(name = "receiver_id")
	private String receiverId;
	@JSONField(name = "sender_id")
	private String senderId;
	private String type;
	private String data;

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public String getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Transient
	public Boolean ISText(){
		return this.type.equals(WBReMsgTypes.TEXT.getType());
	}
	
	@Transient
	public Boolean ISArticles(){
		return this.type.equals(WBReMsgTypes.ARTICLES.getType());
	}
	
	@Transient
	public Boolean ISPosition(){
		return this.type.equals(WBReMsgTypes.POSITION.getType());
	}
	
	@Override
	public String toString() {
		return "WBReMsg [result=" + result + ", receiverId=" + receiverId
				+ ", senderId=" + senderId + ", type=" + type + ", data="
				+ data + "]";
	}
	
	public static void main(String[] args) throws IOException {
		String file = "E:\\eclipse_workspace\\weibo_pay\\resources\\msg.json";
		String json = FileUtils.readFileToString(new File(file), "utf-8");
		
		WBReMsg wb = JSONObject.parseObject(json, WBReMsg.class);
		
		wb.setData(URLEncoder.encode(wb.getData(), "UTF-8"));
		
		System.out.println(wb);
		
		System.out.println(JSONObject.toJSONString(wb, false));
	}
}
