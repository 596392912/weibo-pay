package com.weibo.bean;

/**
 * 消息推送服务-消息回复实体部分
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2014-4-17 上午10:46:23
 */
public class WBReTextMsg {

	// 纯文本类型私信消息：text
	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "WBReTextMsg [text=" + text + "]";
	}
}