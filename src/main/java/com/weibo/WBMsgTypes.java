package com.weibo;

/**
 * 消息类型
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2014-4-17 上午10:33:40
 */
public enum WBMsgTypes {

	// 纯文本类型私信和留言消息：text
	TEXT("text"),
	// 位置类型私信消息：position
	POSITION("position"), 
	// 语音类型私信和留言消息：voice
	VOICE("voice"),
	// 图片类型私信和留言消息：image
	IMAGE("image"),
	// 事件消息：event
	EVENT("event"),
	// 被@消息：mention
	MENTION("mention");
	
	private String type;
	
	WBMsgTypes(String type) {
		this.type = type;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
