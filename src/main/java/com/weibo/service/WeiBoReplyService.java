package com.weibo.service;

import com.weibo.bean.WBMsg;
import com.weibo.bean.WBMsgData;
import com.weibo.bean.WBReMsg;

/**
 * 微博Service
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2014-5-7 下午2:21:43
 */
public interface WeiBoReplyService {

	/**
	 * 
	 * @description 
	 * 功能描述: 纯文本类型私信和留言消息：text
	 * 要回复的私信文本内容。文本大小必须小于300个汉字。 
	 * @author 		  作         者: 卢春梦
	 * @param		  参         数: 
	 * @return       返回类型: 
	 * @createdate   建立日期：2014-4-22下午3:30:54
	 */
	WBReMsg textTypeMsg(WBMsg<WBMsgData> msg);
	
	/**
	 * 
	 * @description 
	 * 功能描述: 位置类型私信消息：position
	 * @author 		  作         者: 卢春梦
	 * @param		  参         数: 
	 * @return       返回类型: 
	 * @createdate   建立日期：2014-4-22下午3:31:28
	 */
	WBReMsg positionTypeMsg(WBMsg<WBMsgData> msg);
	
	/**
	 * 
	 * @description 
	 * 功能描述: 语音类型私信和留言消息：voice
	 * @author 		  作         者: 卢春梦
	 * @param		  参         数: 
	 * @return       返回类型: 
	 * @createdate   建立日期：2014-4-22下午3:31:58
	 */
	WBReMsg voiceTypeMsg(WBMsg<WBMsgData> msg);
	
	/**
	 * 
	 * @description 
	 * 功能描述: 图片类型私信和留言消息：image
	 * @author 		  作         者: 卢春梦
	 * @param		  参         数: 
	 * @return       返回类型: 
	 * @createdate   建立日期：2014-4-22下午3:36:10
	 */
	WBReMsg imageTypeMsg(WBMsg<WBMsgData> msg);
	
	/**
	 * 
	 * @description 
	 * 功能描述: 事件消息：event
	 * @author 		  作         者: 卢春梦
	 * @param		  参         数: 
	 * @return       返回类型: 
	 * @createdate   建立日期：2014-4-22下午3:35:50
	 */
	WBReMsg eventTypeMsg(WBMsg<WBMsgData> msg);
	
	/**
	 * 
	 * @description 
	 * 功能描述: 被@消息：mention
	 * @author 		  作         者: 卢春梦
	 * @param		  参         数: 
	 * @return       返回类型: 
	 * @createdate   建立日期：2014-4-22下午3:35:38
	 */
	WBReMsg mentionTypeMsg(WBMsg<WBMsgData> msg);
}